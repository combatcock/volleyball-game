// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "VolleyballGameInstance.generated.h"

USTRUCT()
struct FMapInfo
{
	GENERATED_BODY()
public:
	UPROPERTY()
	FString MapName;
	UPROPERTY()
	FString MapURL;
};

USTRUCT(BlueprintType)
struct FCreateServerInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite)
	FString ServerName;
	UPROPERTY(BlueprintReadWrite)
	int32 MaxPlayers;
	UPROPERTY(BlueprintReadWrite)
	bool IsLAN;
};

USTRUCT(BlueprintType)
struct FServerInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly)
	FString ServerName;
	UPROPERTY(BlueprintReadOnly)
	int32 CurrentPlayers;
	UPROPERTY(BlueprintReadOnly)
	int32 MaxPlayers;
	UPROPERTY(BlueprintReadOnly)
	int32 ServerPing;
	UPROPERTY(BlueprintReadOnly)
	int32 ServerIndex;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FServerDel, FServerInfo, ServerListDel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FServerSearchingDel, bool, SearchingForServer);

/**
 * 
 */
UCLASS()
class VOLLEYBALL_API UVolleyballGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UVolleyballGameInstance();

protected:
	FName MySessionName;

	UPROPERTY(BlueprintAssignable)
	FServerDel ServerListDel;

	UPROPERTY(BlueprintAssignable)
	FServerSearchingDel SearchingForServer;

	virtual void Init() override;

	IOnlineSessionPtr SessionInterface;

	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	virtual void OnCreateSessionComplete(FName SessionName, bool Succeeded);
	virtual void OnFindSessionComplete(bool Succeeded);
	virtual void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	UFUNCTION(BlueprintCallable)
	void CreateServer(FCreateServerInfo Info);

	UFUNCTION(BlueprintCallable)
	void FindServers(bool IsLAN);

	UFUNCTION(BlueprintCallable)
	void JoinServer(int32 ServerIndex);

private:
	void SetSearchSettings(TSharedPtr<class FOnlineSessionSearch> &SearchSettings);
	
};
