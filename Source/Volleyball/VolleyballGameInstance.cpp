// Fill out your copyright notice in the Description page of Project Settings.


#include "VolleyballGameInstance.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "Kismet/GameplayStatics.h"

UVolleyballGameInstance::UVolleyballGameInstance()
{
    MySessionName = FName("My Session");

    FMapInfo GameMap;
    GameMap.MapName = "";
    GameMap.MapURL = "";
}

void UVolleyballGameInstance::Init()
{
    if (IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get())
    {
        SessionInterface = Subsystem->GetSessionInterface();
        if (SessionInterface)
        {
            //Bind Delegates Here
            SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UVolleyballGameInstance::OnCreateSessionComplete);
            SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UVolleyballGameInstance::OnFindSessionComplete);
            SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UVolleyballGameInstance::OnJoinSessionComplete);
        }
    }
}

void UVolleyballGameInstance::OnCreateSessionComplete(FName SessionName, bool Succeeded)
{
    FString MapName = "/Game/Levels/Game?listen";
    if (Succeeded)
    {
        GetWorld()->ServerTravel(MapName);
    }
}

void UVolleyballGameInstance::OnFindSessionComplete(bool Succeeded)
{
    if (Succeeded)
    {   
        int32 ArrayIndex = -1;
        for (FOnlineSessionSearchResult Result : SessionSearch->SearchResults)
        {
            ++ArrayIndex;
            if (!Result.IsValid()) continue;

            FServerInfo ServerInfo;
            FString ServerName = "Empty";
            FString HostName = "Empty";

            Result.Session.SessionSettings.Get(FName("SERVER_NAME_KEY"), ServerName);
            Result.Session.SessionSettings.Get(FName("SERVER_HOSTNAME_KEY"), HostName);

            ServerInfo.ServerName = ServerName;
            ServerInfo.MaxPlayers = Result.Session.SessionSettings.NumPublicConnections;
            ServerInfo.CurrentPlayers = ServerInfo.MaxPlayers-Result.Session.NumOpenPublicConnections;
            ServerInfo.ServerIndex = ArrayIndex;

            ServerListDel.Broadcast(ServerInfo);
        }
    }
    SearchingForServer.Broadcast(false);
}

void UVolleyballGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
    UE_LOG(LogTemp, Warning, TEXT("Session name: %s"), *SessionName.ToString());
    if (APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0))
    {
        FString JoinAdress = "";
        SessionInterface->GetResolvedConnectString(SessionName, JoinAdress);
        if (JoinAdress != "")
        {
            PlayerController->ClientTravel(JoinAdress, ETravelType::TRAVEL_Absolute);
        }
    }
}

void UVolleyballGameInstance::CreateServer(FCreateServerInfo Info)
{
    FOnlineSessionSettings SessionSettings;

    SessionSettings.bAllowJoinInProgress = true;
    SessionSettings.bIsDedicated = false;
    SessionSettings.bUseLobbiesIfAvailable = true;
    SessionSettings.bAllowJoinViaPresence = true;
    SessionSettings.bShouldAdvertise = true;
    SessionSettings.bUsesPresence = true;

    SessionSettings.Set(FName("SERVER_NAME_KEY"), Info.ServerName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
    SessionSettings.NumPublicConnections = Info.MaxPlayers;
    SessionSettings.bIsLANMatch = Info.IsLAN;

    SessionInterface->CreateSession(0, MySessionName, SessionSettings);
}

void UVolleyballGameInstance::FindServers(bool IsLAN)
{
    SearchingForServer.Broadcast(true);
    SessionSearch = MakeShareable(new FOnlineSessionSearch());

    SessionSearch->bIsLanQuery = IsLAN;
    SessionSearch->MaxSearchResults = 10000;
    SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

    SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
    
}

void UVolleyballGameInstance::JoinServer(int32 ServerIndex)
{
    FOnlineSessionSearchResult Result = SessionSearch->SearchResults[ServerIndex];
    if (Result.IsValid())
    {
        SessionInterface->JoinSession(0, MySessionName, Result);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("FAILED TO JOIN SERVER AT INDEX: %d"), ServerIndex);
    }
}

void UVolleyballGameInstance::SetSearchSettings(TSharedPtr<class FOnlineSessionSearch> &SearchSettings)
{
    bool IsLanMatch = (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL");
    SearchSettings->bIsLanQuery = IsLanMatch;
    SearchSettings->MaxSearchResults = 10000;
    SearchSettings->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
}
