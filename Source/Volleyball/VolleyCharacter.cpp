// Fill out your copyright notice in the Description page of Project Settings.


#include "VolleyCharacter.h"
#include "Components/InputComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputAction.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
AVolleyCharacter::AVolleyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));
	SpringArmComp->SetupAttachment(RootComponent);
	SpringArmComp->TargetArmLength = 1000;
	SpringArmComp->SetUsingAbsoluteRotation(true);
	SpringArmComp->bUsePawnControlRotation = false;
	SpringArmComp->AddWorldRotation(FRotator(0, -90, 0));

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Player Camera"));
	CameraComp->SetupAttachment(SpringArmComp);
	CameraComp->bUsePawnControlRotation = false;

}

//Called when the game starts or when spawned
void AVolleyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AVolleyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AVolleyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* Input = Cast<UEnhancedInputComponent>(InputComponent))
	{
		Input->BindAction(MoveInputAction, ETriggerEvent::Triggered, this, &AVolleyCharacter::Move);
		Input->BindAction(JumpInputAction, ETriggerEvent::Triggered, this, &ACharacter::Jump);
		Input->BindAction(DashInputAction, ETriggerEvent::Triggered, this, &AVolleyCharacter::Dash);
	}

}

void AVolleyCharacter::Move(const FInputActionInstance& Instance)
{
	const float MovementVector = Instance.GetValue().Get<float>();

	const FRotator Rotation = CameraComp->GetRelativeRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);
	const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

	AddMovementInput(ForwardDirection, MovementVector);
}

void AVolleyCharacter::Dash(const FInputActionInstance& Instance)
{
}
