// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuHUD.h"
#include "Blueprint/UserWidget.h"

void AMainMenuHUD::BeginPlay()
{
    UUserWidget* HUDWidget = CreateWidget<UUserWidget>(GetOwningPlayerController(), MainMenuWidget);
    if (!HUDWidget) return;
    HUDWidget->AddToViewport();

    GetOwningPlayerController()->SetShowMouseCursor(true);
    GetOwningPlayerController()->SetInputMode(FInputModeUIOnly());
}
